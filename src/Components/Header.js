import React, { Component } from 'react';
import Navbar from './Navbar';

class Header extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <div> This is the Header Component </div>
      </div>
    );
  }
}

export default Header;