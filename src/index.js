import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Dashboard from './Dashboard';
import registerServiceWorker from './registerServiceWorker';
import {
  Switch,
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Header from './Components/Header';

const Login = ()=> {
  return (<div> 
    {/*<Route path='/login' component={Header}></Route>*/}
    this is the Login component 
  </div>);
};

const Signup = ()=>{
  return (
    <div> 
      {/*<Route path='/signup' component={Header}></Route>*/}
      this is the Signup component 
    </div>
    );
};

const Root = () => {
  return(
    <Router>
      <div>
        <div>
          <Route path='/' component={Header}></Route>
          <Route exact path="/" component = {Dashboard} />
          <Route path="/login" component = {Login} />
          <Route path="/signup" component = {Signup} />
        </div>
      </div>
    </Router>
  );
}

ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();


//ref : https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/guides/migrating.md#nesting-routes
//ref : https://medium.com/@marxlow/simple-nested-routes-with-layouts-in-react-router-v4-59b8b63a1184 
